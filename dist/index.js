/**
 * clever-ui
 *
 * 支持CMD模块化/ 支持传统引入
 * @author cleverstone
 */

(function (window, $, noGlobal) {
    /* 构造函数 */
    var CleverUiConstructor = function () {
    }

    // clever-ui对象
    var $cui = new CleverUiConstructor()

    // 继承方法
    $cui.extend = function (any) {
        for (var attr in any) {
            if ("undefined" != typeof CleverUiConstructor.prototype[attr]) {
                throw `方法${attr}已存在. `
            } else {
                CleverUiConstructor.prototype[attr] = any[attr]
            }
        }
    }

    // 执行方法
    $cui.extend({
        run: function () {
            // 执行ui事件监听
            this.registerEvent()

            // 执行jQuery扩展
            this.jQueryExtend()

            return this
        }
    })

    // 注册Event事件
    $cui.extend({
        registerEvent: function () {
            // 事件委托
            $(document).on('click', '[data-cui-close]', function (e) {
                var toggle = $(this).data('cui-close')
                switch (toggle) {
                    case 'cuimodal':
                        /* 模态框关闭事件 */
                        $(this).parents('.cui-modal').cuiModal('hide')
                        break
                }
            }).on('click', '[data-cui-toggle]', function (e) {
                var toggle = $(this).data('cui-toggle'),
                    target
                switch (toggle) {
                    case 'cuimodal':
                        /* 模态框打开事件 */
                        target = $(e.currentTarget).data('cui-target')
                        $(target).cuiModal('show')
                        break
                    case 'cuitab':
                        /* 标签页 */
                        $(this).cuiTab('show')
                        break
                    case 'cuiaside':
                        target = $(e.currentTarget).data('cui-target')
                        $(target).cuiAside('show')
                        break
                    case 'cuidropdown':
                        // 阻止事件冒泡
                        e.stopPropagation()
                        if ($(this).parent().hasClass('cui-open')) {
                            $(this).parent().removeClass('cui-open')
                        } else {
                            $('.cui-dropdown.cui-open').removeClass('cui-open')
                            $(this).parent().addClass('cui-open')
                        }
                        break
                }
            }).on('click', '.cui-modal', function (e) {
                var target = e.target
                /* 遮罩框点击关闭模态事件 */
                if (
                    $(this).data('cui-shade') !== false
                    && $(target).hasClass('cui-modal')
                ) {
                    $(this).cuiModal('hide')
                }
            }).on('click', '.cui-aside', function (e) {
                var target = e.target
                if (
                    $(this).data('cui-shade') !== false
                    && $(target).hasClass('cui-aside')
                ) {
                    $(this).cuiAside('hide')
                }
            }).on('click', '[data-cui-href]', function (e) {
                var url = $(this).data('cui-href') || '/',
                    target = $(this).data('cui-target') || '_self'
                window.open(url, target)
            }).on('click', function () {
                // 关闭所有下拉菜单
                $('.cui-dropdown.cui-open').removeClass('cui-open')
            })

            return this
        }
    })

    // jQuery扩展
    $cui.extend({
        jQueryExtend: function () {
            $.fn.extend({
                /* 模态框 */
                cuiModal(event, callback) {
                    event = event || 'show'
                    callback = callback || function () {
                    }
                    var eleBody = $('body')
                        , that = this
                        , dismissObj = $('.cui-bg-dismiss')
                    switch (event) {
                        case 'show':
                            that.css('display', 'flex')
                            /* 立即触发cui modal显示事件 */
                            that.trigger('cui.modal.show')
                            var timer1 = setTimeout(function () {
                                clearTimeout(timer1)
                                that.addClass('cui-show')
                                callback.apply(that)
                                /* 触发cui modal显示成功事件 */
                                that.trigger('cui.modal.shown')
                            }, 50)

                            if (dismissObj.length <= 0) {
                                that.attr('cui-dismiss-modal', true)
                                var dismissJquery = $('<div class="cui-bg-dismiss cui-dismiss-fade"></div>')
                                eleBody.append(dismissJquery)
                                var timer3 = setTimeout(function () {
                                    clearTimeout(timer3)
                                    dismissJquery.addClass('cui-in')
                                }, 50)
                            }

                            return that
                        case 'hide':
                            that.removeClass('cui-show')
                            that.trigger('cui.modal.hide')
                            var timer2 = setTimeout(function () {
                                clearTimeout(timer2)
                                that.css('display', 'none')
                                callback.apply(that)
                                that.trigger('cui.modal.hidden')
                            }, 150)

                            if (that.attr('cui-dismiss-modal')) {
                                that.removeAttr('cui-dismiss-modal')
                                dismissObj.removeClass('cui-in')
                                var timer4 = setTimeout(function () {
                                    clearTimeout(timer4)
                                    dismissObj.remove()
                                }, 200)
                            }

                            return that
                        default:
                            throw 'cuiModal() Undefined param' + event
                    }
                },
                /* Loading hide */
                cuiLoadingHide() {
                    this.trigger('cui.loading.hide')

                    return this
                },
                /* btnloading show */
                cuiBtnLoading() {
                    var originTxt = this.html(),
                        loadingTxt = this.data('cui-btnloadingtext') || '处理中...'
                    this.html(loadingTxt).css({'opacity': '.7', 'cursor': 'not-allowed'}).prop('disabled', true)
                    var _ = this
                    this.on('cui.btnloading.reset', function (e) {
                        _.html(originTxt).css({'opacity': '', 'cursor': ''}).prop('disabled', false)
                    })

                    return true
                },
                /* btnloading reset */
                cuiBtnLoadingReset() {
                    this.trigger('cui.btnloading.reset')

                    return false
                },
                cuiTab(event, callback) {
                    var that = this,
                        tabTarget = that.data('cui-target')
                    if (!tabTarget) {
                        throw 'No found' + tabTarget
                    }

                    var tabTargetObj = $(tabTarget)
                    event = event || 'show'
                    callback = callback || function (object) {
                    }
                    switch (event) {
                        case 'show':
                            if (that.hasClass('cui-tab-active')) {
                                return that
                            }

                            that.addClass('cui-tab-active')
                            that.siblings('.cui-tab-active').cuiTab('hide', function (obj) {
                                tabTargetObj.addClass('cui-tab-active')
                                that.trigger('cui.tab.show')
                                var timer = setTimeout(function () {
                                    clearTimeout(timer)
                                    tabTargetObj.addClass('cui-tab-in')
                                    callback.call(that)
                                    that.trigger('cui.tab.shown')
                                }, 50)
                            })

                            return that
                        case 'hide':
                            if (!that.hasClass('cui-tab-active')) {
                                return that
                            }

                            that.removeClass('cui-tab-active')
                            tabTargetObj.removeClass('cui-tab-in')
                            that.trigger('cui.tab.hide')
                            var timer = setTimeout(function () {
                                clearTimeout(timer)
                                tabTargetObj.removeClass('cui-tab-active')
                                callback.call(that)
                                that.trigger('cui.tab.hidden')
                            }, 200)

                            return that
                    }
                },
                // 侧边框
                cuiAside(event, callback) {
                    event = event || 'show'
                    callback = callback || function (obj) {
                    }
                    switch (event) {
                        case 'show':
                            this.trigger('cui.aside.show')
                            var timer2 = setTimeout(function () {
                                clearTimeout(timer2)
                                this.css({'display': 'block'}).find('.cui-aside-dialog').addClass('cui-slidein')
                                callback.call(this)
                                this.trigger('cui.aside.shown')
                            }.bind(this), 50)
                            return this
                        case 'hide':
                            this.find('.cui-aside-dialog').addClass('cui-slideout')
                            this.trigger('cui.aside.hide')
                            var timer = setTimeout(function () {
                                clearTimeout(timer)
                                this.css({'display': 'none'})
                                    .find('.cui-aside-dialog')
                                    .removeClass('cui-slideout')
                                    .removeClass('cui-slidein')
                                callback.call(this)
                                this.trigger('cui.aside.hidden')
                            }.bind(this), 200)
                            return this
                    }
                },
                // select
                cuiSelect() {
                    var _ = this,
                        w = _[0].offsetWidth, // 宽
                        h = _[0].offsetHeight, // 高
                        showVal = '', // 选中的文本
                        template = '<div class="cui-select">\n',
                        domLi = (function () {
                            var tempStr = '';
                            _.find('option').each(function (index) {
                                if (!index) {
                                    showVal = this.innerText
                                }
                                if (
                                    _.val() === this.value
                                    && this.value !== ''
                                ) {
                                    showVal = this.innerText
                                    tempStr += '<li class="cui-select-item cui-select--active" data-cui-value="' + this.value + '">' + this.innerText + '</li>\n'
                                } else if (this.hasAttribute('disabled')) {
                                    tempStr += '<li class="cui-select-item cui-select--disabled" data-cui-value="' + this.value + '">' + this.innerText + '</li>\n'
                                } else {
                                    tempStr += '<li class="cui-select-item" data-cui-value="' + this.value + '">' + this.innerText + '</li>\n'
                                }
                            })

                            return tempStr
                        })()
                    template += '<div class="cui-select-header">\n'
                    template += '<input class="cui-select-header--item" value="' + showVal + '" readonly>'
                    template += '<i class="cui-select-header--item--icon cui-select-icon-down"></i>\n'
                    template += '</div>\n'
                    template += '<div class="cui-select-content cui-hidden">\n'
                    template += '<ul class="cui-select--content">\n'
                    template += domLi
                    template += '</ul>\n'
                    template += '</div>\n'
                    template += '</div>' // 模板
                    var jQueryTemplate = $(template)
                    jQueryTemplate.css({
                        width: w + 'px',
                        height: h + 'px'
                    })
                    _.css('display', 'none') // 隐藏元素
                    _.after(jQueryTemplate)
                    // 点击选择框事件
                    jQueryTemplate.on('click', '.cui-select-header', function (e) {
                        var contentObj = $(this).siblings('.cui-select-content')
                        if (contentObj.hasClass('cui-hidden')) {
                            contentObj.removeClass('cui-hidden')
                        } else {
                            contentObj.addClass('cui-hidden')
                        }
                    }).on('click', '.cui-select-item', function (e) {
                        // 点击选择框项目事件

                        // 项目被禁用
                        if ($(this).hasClass('cui-select--disabled')) {
                            return false
                        }

                        // 隐藏下拉内容
                        var contentObj = jQueryTemplate.find('.cui-select-content')
                        contentObj.addClass('cui-hidden')
                        // 选中项目
                        $(this).addClass('cui-select--active').siblings().removeClass('cui-select--active')
                        // 展示选中的值
                        var t = $(this).text()
                        jQueryTemplate.find('.cui-select-header--item').val(t)
                        // 改变select的值
                        var v = $(this).data('cui-value')
                        _.val(v).trigger('change')
                    })

                    return this
                },

            })
        }
    })

    // 内置组件库
    $cui.extend({
        /**
         * 滚动条
         * @param scrollEleId
         * @param scrollThumbEleId
         * @param scrollBarEleId
         */
        scrollY: function (scrollEleId, scrollThumbEleId, scrollBarEleId) {
            var scrollObj = $('#' + scrollEleId), // 菜单
                scrollBarObj = $('#' + scrollBarEleId), // 滚动条
                scrollThumbObj = $('#' + scrollThumbEleId), // 滚动槽
                hideTimer, // 滚动条静止后隐藏定时器
                minMoveInstance = 0, // 滚动条最小滚动距离
                maxMoveInstance = scrollThumbObj.height() - scrollBarObj[0].offsetHeight, // 滚动条最大滚动距离
                initInstance = 0, // 滚动条滚动距离
                nowLocation = 0, // 滚动条当前Y轴坐标
                moveDistance = 0, // 滚动条实时滚动距离
                mouseMoveListener, // mousemove事件监听器
                maxScrollTop = scrollObj[0].scrollHeight - scrollObj[0].clientHeight, // 最大滚动高度
                scrollRate = maxScrollTop === 0 ? 0 : (maxMoveInstance / maxScrollTop) // 滚动比例
            var setScrollBarShow = function () {
                clearTimeout(hideTimer)
                if (maxScrollTop !== 0) {
                    scrollBarObj.css('opacity', 1)
                }
            };
            var setScrollBarHide = function () {
                clearTimeout(hideTimer)
                hideTimer = setTimeout(function () {
                    clearTimeout(hideTimer)
                    scrollBarObj.css('opacity', 0)
                }, 1000)
            }
            var setScrollBarFlushShow = function () {
                setScrollBarHide()
                if (maxScrollTop !== 0) {
                    scrollBarObj.css('opacity', 1)
                }
            }
            // 监听插槽hover事件
            var mouseEnterListener = function (e) {
                setScrollBarShow()
            }
            var mouseLeaveListener = function (e) {
                setScrollBarHide()
            }
            scrollThumbObj.hover(mouseEnterListener, mouseLeaveListener)
            // 监听菜单滚动
            var scrollListener = function (e) {
                setScrollBarFlushShow()
                var _ = this,
                    __ = $(this)
                moveDistance = scrollRate === 0 ? maxMoveInstance : (this.scrollTop * scrollRate)
                if (moveDistance <= minMoveInstance) {
                    moveDistance = minMoveInstance
                }

                if (moveDistance >= maxMoveInstance) {
                    moveDistance = maxMoveInstance
                }

                // 重新记录滚动条滚动距离
                initInstance = moveDistance
                // 渲染
                scrollBarObj.css('transform', 'translateY(' + moveDistance + 'px)')
            }
            scrollObj.on('scroll', scrollListener)
            // 监听滚动条鼠标点击
            scrollBarObj.mousedown(function (e) {
                scrollThumbObj.off('mouseenter', mouseEnterListener).off('mouseleave', mouseLeaveListener)
                setScrollBarShow()
                scrollObj.off('scroll', scrollListener)
                e.preventDefault()
                var _ = this,
                    __ = $(this),
                    originLocation = e.clientY
                mouseMoveListener = function (e) {
                    // 滚动条实时Y轴坐标
                    nowLocation = e.clientY
                    // 滚动条实际Y轴偏移量
                    moveDistance = initInstance + (nowLocation - originLocation)
                    if (moveDistance <= minMoveInstance) {
                        moveDistance = minMoveInstance
                    }

                    if (moveDistance >= maxMoveInstance) {
                        moveDistance = maxMoveInstance
                    }

                    // 渲染滚动条滚动
                    __.css('transform', 'translateY(' + moveDistance + 'px)')
                    // 渲染菜单滚动
                    scrollObj[0].scrollTop = scrollRate === 0 ? maxScrollTop : (moveDistance / scrollRate)
                }
                // 监听滚动条鼠标移动
                window.addEventListener('mousemove', mouseMoveListener)
            })
            // 监听浏览器鼠标弹起
            window.addEventListener('mouseup', function (e) {
                if (
                    e.target.id !== scrollThumbEleId
                    && e.target.id !== scrollBarEleId
                ) {
                    setScrollBarHide()
                }
                // 重新监听滚动条hover
                scrollThumbObj.hover(mouseEnterListener, mouseLeaveListener)
                // 重新记录滚动条滚动距离
                initInstance = moveDistance
                // 移除鼠标移动事件
                window.removeEventListener('mousemove', mouseMoveListener)
                // 绑定滚动事件
                scrollObj.on('scroll', scrollListener)
            });
            // 监听window窗口变化
            window.addEventListener('resize', function (e) {
                // 获取最大菜单滚动高度
                maxScrollTop = scrollObj[0].scrollHeight - scrollObj[0].clientHeight
                // 重新记录滚动条最大滚动距离
                maxMoveInstance = scrollThumbObj.height() - scrollBarObj[0].offsetHeight
                // 重新记录滚动比例
                scrollRate = maxScrollTop === 0 ? 0 : (maxMoveInstance / maxScrollTop)
                moveDistance = scrollRate === 0 ? maxMoveInstance : (scrollObj[0].scrollTop * scrollRate)
                if (moveDistance <= minMoveInstance) {
                    moveDistance = minMoveInstance
                }

                if (moveDistance >= maxMoveInstance) {
                    moveDistance = maxMoveInstance
                }

                // 重新记录滚动条滚动距离
                initInstance = moveDistance
                // 重新渲染页面
                scrollBarObj.css('transform', 'translateY(' + moveDistance + 'px)')
            })
        },

        /**
         * 轻提示
         */
        toastr: {
            // 提示
            // size: sm, md, lg
            tip(message, size, second) {
                message = message || '友情提示！'
                this.handle(message, size, second, 'tip')
            },
            // 信息
            info(message, size, second) {
                message = message || '重要信息！'
                this.handle(message, size, second, 'info')
            },
            // 警告
            warning(message, size, second) {
                message = message || '危险警告！'
                this.handle(message, size, second, 'warning')
            },
            // 错误
            error(message, size, second) {
                message = message || '系统错误！'
                this.handle(message, size, second, 'error')
            },
            handle(message, size, second, type) {
                var classname,
                    title
                size = size || 'sm'
                second = (second === undefined || second == null) ? 2.5 : parseFloat(second)
                switch (type) {
                    case 'tip':
                        classname = 'cui-toastr-default'
                        title = '<i class="cui-icon icon-tishi"></i>'
                        break;
                    case 'info':
                        classname = 'cui-toastr-info'
                        title = '<i class="cui-icon icon-xinxi"></i>'
                        break;
                    case 'warning':
                        classname = 'cui-toastr-warning'
                        title = '<i class="cui-icon icon-jinggao"></i>'
                        break;
                    case 'error':
                        classname = 'cui-toastr-danger'
                        title = '<i class="cui-icon icon-cuowu"></i>'
                        break;
                    default:
                        classname = 'cui-toastr-default'
                        title = '<i class="cui-icon icon-tishi"></i>'
                }

                var template = '<div class="cui-toastr cui-toastr-transparent cui-toastr-' + size + ' ' + classname + '">\n'
                template += '<i data-cui-toastr="close" class="cui-toastr-close">&times;</i>\n'
                template += '<h4>' + title + '</h4>\n'
                template += '<p>' + message + '</p>\n'
                template += '</div>\n'
                var jQueryTemplateContainer = $('<div class="cui-toastr-container"></div>')
                    , jQueryTemplate = $(template)
                    , toastrContainer = $('.cui-toastr-container')
                    , timerSet = function () {
                    var timerLocal = setTimeout(function () {
                        clearTimeout(timerLocal)
                        if (jQueryTemplate.length > 0) {
                            jQueryTemplate.removeClass('cui-toastr-fade')
                            var timerInnerLocal = setTimeout(function () {
                                clearTimeout(timerInnerLocal)
                                jQueryTemplate.remove()
                                if ($('.cui-toastr-container>.cui-toastr').length <= 0) {
                                    $('.cui-toastr-container').remove()
                                }
                            }, 200)
                        }
                    }, second * 1000)

                    return timerLocal
                }

                if (toastrContainer.length <= 0) {
                    $('body').prepend(jQueryTemplateContainer)
                    jQueryTemplateContainer.prepend(jQueryTemplate)
                } else {
                    toastrContainer.prepend(jQueryTemplate)
                }

                var animateTimer = setTimeout(function () {
                    clearTimeout(animateTimer)
                    jQueryTemplate.addClass('cui-toastr-fade')
                }, 50)

                var timer
                if (!!second) {
                    timer = timerSet()
                    jQueryTemplate.hover(function (e) {
                        // mouseenter
                        clearTimeout(timer)
                    }, function () {
                        // mouseleave
                        timer = timerSet()
                    })
                }

                // 点击关闭
                jQueryTemplate.find('[data-cui-toastr]').on('click', function (e) {
                    var dataToastrVal = $(this).data('cui-toastr')
                    switch (dataToastrVal) {
                        case 'close':
                            if (timer) {
                                clearTimeout(timer)
                            }

                            if (jQueryTemplate.length > 0) {
                                jQueryTemplate.remove()
                                var cuiToastrContainer = $('.cui-toastr-container')
                                if (
                                    cuiToastrContainer.length > 0
                                    && $('.cui-toastr-container>.cui-toastr').length <= 0
                                ) {
                                    cuiToastrContainer.remove()
                                }
                            }

                            break
                    }
                })
            }
        },

        /**
         * loading动画
         * @param animateType
         * @param message
         * @param dismissTransparent
         * @returns {jQuery.fn.init|jQuery|HTMLElement}
         */
        loadingShow: function (animateType, message, dismissTransparent) {
            animateType = animateType || 'water'
            message = message || '正在处理，请稍后...'
            var dismissBackground = dismissTransparent ? 'background-color: rgba(0, 0, 0, 0)' : 'background-color: rgba(0, 0, 0, .3)'
            var loadingTemp = '<div class="cui-loading-dismiss" style="' + dismissBackground + '">\n'
            loadingTemp += '<div class="cui-loading-wrap cui-scalein">\n'
            switch (animateType) {
                case 'water':
                    loadingTemp += '<div class="cui-loading-water">\n'
                    loadingTemp += '<div class="cui-bounce1"></div>\n'
                    loadingTemp += '<div class="cui-bounce2"></div>\n'
                    loadingTemp += '<div class="cui-bounce3"></div>\n'
                    loadingTemp += '</div>\n'
                    break
                case 'scaleout':
                    loadingTemp += '<div class="cui-loading-scaleout"></div>\n'
                    break;
                case 'inout':
                    loadingTemp += '<div class="cui-loading-inout">\n'
                    loadingTemp += '<div class="cui-double-bounce1"></div>\n'
                    loadingTemp += '<div class="cui-double-bounce2"></div>\n'
                    loadingTemp += '</div>\n'
                    break
                case '3d':
                    loadingTemp += '<div class="cui-loading-3d"></div>\n'
                    break
                case 'downup':
                    loadingTemp += '<div class="cui-loading-downup">\n'
                    loadingTemp += '<div class="cui-rect1"></div>\n'
                    loadingTemp += '<div class="cui-rect2"></div>\n'
                    loadingTemp += '<div class="cui-rect3"></div>\n'
                    loadingTemp += '<div class="cui-rect4"></div>\n'
                    loadingTemp += '<div class="cui-rect5"></div>\n'
                    loadingTemp += '</div>\n'
                    break
            }

            loadingTemp += '<p class="cui-loading-txt">\n'
            loadingTemp += message + '\n'
            loadingTemp += '</p>\n'
            loadingTemp += '</div>\n'
            loadingTemp += '</div>\n'
            var currentJqueryLoading = $(loadingTemp)
            $('body').append(currentJqueryLoading)
            currentJqueryLoading.on('cui.loading.hide', function (e) {
                var _ = $(this)
                _.css('backgroundColor', 'rgba(0,0,0,0)')
                _.find('.cui-loading-wrap')
                    .removeClass('cui-scalein')
                    .addClass('cui-scaleout')
                var timer = setTimeout(function () {
                    clearTimeout(timer)
                    _.remove()
                }, 200)
            })

            return currentJqueryLoading
        },

        /**
         * 确认框
         * @param content
         * @param callback
         */
        confirm: function (content, callback) {
            callback = callback || function (closeFunc) {
                alert('confirm 回调')
                closeFunc()
            }
            var bodyObj = $('body'),
                confirmTemplate = '<div class="cui-confirm-container">\n'
            confirmTemplate += '<div class="cui-confirm cui-confirm-transparent">\n'
            confirmTemplate += '<div class="cui-confirm-content">\n'
            confirmTemplate += content
            confirmTemplate += '</div>\n'
            confirmTemplate += '<div class="cui-confirm-footer">\n'
            confirmTemplate += '<button type="button" data-cuiconfirm="sure" class="cui-btn cui-btn-primary">确定</button>\n'
            confirmTemplate += '<button type="button" data-cuiconfirm="cancal" class="cui-btn cui-btn-white">取消</button>\n'
            confirmTemplate += '</div>\n'
            confirmTemplate += '</div>\n'
            confirmTemplate += '</div>\n'
            var jQueryConfirm = $(confirmTemplate),
                closeCurrentConfirm = function () {
                    jQueryConfirm.find('.cui-confirm').removeClass('cui-confirm-fade')
                    var innerTimer = setTimeout(function () {
                        clearTimeout(innerTimer)
                        jQueryConfirm.remove()
                    }, 200)
                }

            bodyObj.prepend(jQueryConfirm)
            var confirmTimer = setTimeout(function () {
                clearTimeout(confirmTimer)
                jQueryConfirm.find('.cui-confirm').addClass('cui-confirm-fade')
            }, 50)
            jQueryConfirm.find('[data-cuiconfirm="sure"]').on('click', function (e) {
                callback.call($cui, closeCurrentConfirm, $(this))
            })

            jQueryConfirm.find('[data-cuiconfirm="cancal"]').on('click', function (e) {
                closeCurrentConfirm()
            })
        }
    })

    if (!noGlobal) {
        window.$cui = $cui.run()
    } else {
        module.exports = $cui.run()
    }

}(window, (function (w) {
    if (w.jQuery || w.$) {
        return w.jQuery || w.$
    } else if ("object" === typeof module && "object" === typeof module.exports) {
        try {
            return require("jquery")
        } catch (e) {
            throw e
        }
    } else {
        throw 'no Found jQuery. '
    }
}(window)), (function () {
    return "object" === typeof module && "object" === typeof module.exports
}())))

